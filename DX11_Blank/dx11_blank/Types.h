#pragma once

using byte = unsigned char;
constexpr nullptr_t null = nullptr;
using wchar = wchar_t;
using tchar = TCHAR;
using uint = unsigned int;
using uint32 = uint32_t;
using uint64 = uint64_t;
