//////////////////////////////////////////////////////////////////////
// DX11 Blank App

#include "Common.h"
#include <D3D11.h>
#include <xinput.h>

#pragma comment(lib, "D3D11.lib")

//////////////////////////////////////////////////////////////////////
// command line args
//
// parameter                type            default
//
// -log_level               [int]           3               0 = no logging at all, 1 = errors only, 5 = lots of logging (it goes to .\log.txt)
// -triple_buffered         [flag]          false           double buffered by default
// -fullscreen              [flag]          false           windowed by default
// -debug                   [flag]          false           D3D11 debug mode, messages go to log.txt (untested)
// -swap_effect=flip_type   [flip_type]     discard         can be one of [discard|sequential|flip_sequential|flip_discard]
// -vsync=N                 [int]           1               0 = no vsync (tearing in fullscreen), 1 = 60Hz, 2 = 30Hz and so on
// -flash_buttons=ABXY      [button_mask]   ABXY            which pad buttons cause screen to flash, ABXY are the only options
// -blank_color=RRGGBB      [color]         000000          RRGGBB
// -flash_color=RRGGBB      [color]         FFFFFF          RRGGBB
// -window_size=1280x720    [window_size]   1280x720        this is the client size in windowed mode or resolution in fullscreen

//////////////////////////////////////////////////////////////////////

namespace {

LOG_Context("main");

//////////////////////////////////////////////////////////////////////

auto const pad_a = XINPUT_GAMEPAD_A;
auto const pad_b = XINPUT_GAMEPAD_B;
auto const pad_x = XINPUT_GAMEPAD_X;
auto const pad_y = XINPUT_GAMEPAD_Y;

auto const all_buttons = pad_a | pad_b | pad_x | pad_y;

struct params_t
{
    int log_level = 0;
    bool triple_buffered = false;
    bool fullscreen = false;
    bool debug = false;
    DXGI_SWAP_EFFECT swap_effect = DXGI_SWAP_EFFECT_DISCARD;
    int vsync_frames = 1;
    uint32 flash_buttons = all_buttons;
    uint32 blank_color = 0;
    uint32 flash_color = 0xffffff;
    int window_width = 1280;
    int window_height = 720;
} params;

//////////////////////////////////////////////////////////////////////

struct LoggerAdmin
{
    LOG_Context("Logger");

    static inline wchar const *log_filename()
    {
        return L"log.txt";
    }

    LoggerAdmin()
    {
        Log_SetLevel(None);
        LOG_Info("Logging begins");
    }

    ~LoggerAdmin()
    {
        LOG_Info("Log file closing...");
        Log_Close();
    }

} LoggerAdminInstance;

//////////////////////////////////////////////////////////////////////
// load XInput1_3.dll dynamically because it's not installed on Windows Server 2012 (package must copy it locally to application folder)

struct XInput
{
    LOG_Context("XInput");

    typedef DWORD(WINAPI *XInputGetState_t)(DWORD, XINPUT_STATE *);

    static bool opened;
    static HMODULE XInputDll;
    static XInputGetState_t XInputGetStatePtr;

    static XInputGetState_t XInputGetState_fn()
    {
        if(!opened) {
            XInputDll = LoadLibrary(TEXT("xinput1_3.DLL"));
            XInputGetStatePtr = reinterpret_cast<XInputGetState_t>(GetProcAddress(XInputDll, "XInputGetState"));
            opened = true;
            if(XInputGetStatePtr == null) {
                LOG_Warn("XInput open failed: DLL: %p, XInputGetState: %p", XInputDll, XInputGetStatePtr);
            }
        }
        return XInputGetStatePtr;
    }

    static void cleanup()
    {
        XInputGetStatePtr = null;
        if(XInputDll != null) {
            FreeLibrary(XInputDll);
            LOG_Info("Closing XInput DLL");
        }
        opened = false;
    }

    static DWORD WINAPI GetState(DWORD d, XINPUT_STATE *state)
    {
        auto p = XInputGetState_fn();
        if(p != null) {
            return p(d, state);
        }
        return 0;
    }
};

HMODULE XInput::XInputDll;
XInput::XInputGetState_t XInput::XInputGetStatePtr;
bool XInput::opened = false;

//////////////////////////////////////////////////////////////////////

HINSTANCE hInst = null;
HWND hWnd = null;

wchar const *class_name = L"DX11_Blank";
wchar const *window_text = L"DX11_Blank";

float blank_color[4];
float flash_color[4];

//////////////////////////////////////////////////////////////////////

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch(msg) {
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;
    case WM_KEYDOWN:
        if(wParam == VK_ESCAPE) {
            DestroyWindow(hWnd);
        }
        return 0;
    case WM_NCHITTEST: {
        LRESULT r = DefWindowProc(hWnd, msg, wParam, lParam);
        if(r == HTCLIENT) {
            r = HTCAPTION;    // drag window in the client area so moving small window around is easier
        }
        return r;
    }
    default:
        return DefWindowProc(hWnd, msg, wParam, lParam);
        break;
    }
}

//////////////////////////////////////////////////////////////////////

bool InitWindow(HINSTANCE hInstance)
{
    LOG_Context("InitWindow");
    LOG_Debug("InitWindow begins");

    hInst = hInstance;
    WNDCLASSEX wndclass{ 0 };
    wndclass.cbSize = sizeof(wndclass);
    wndclass.lpszClassName = class_name;
    wndclass.hInstance = hInstance;
    wndclass.lpfnWndProc = WndProc;
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    RegisterClassEx(&wndclass);

    RECT rc{ 0, 0, params.window_width, params.window_height };
    AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
    int w = rc.right - rc.left;
    int h = rc.bottom - rc.top;

    LOG_Debug("Creating Window: %d,%d", w, h);
    hWnd = CreateWindow(wndclass.lpszClassName, window_text, WS_OVERLAPPEDWINDOW, 100, 100, w, h, NULL, NULL, hInstance, NULL);
    LOG_Debug("HWND is %p", hWnd);
    if(hWnd == NULL) {
        LOG_Debug("CreateWindow failed, exiting");
        return false;
    }
    return true;
}

//////////////////////////////////////////////////////////////////////

namespace D3D {

LOG_Context("D3D");

IDXGISwapChain *swap_chain = null;
ID3D11Device *device = null;
ID3D11DeviceContext *context = null;
ID3D11Texture2D *backbuffer = null;
ID3D11RenderTargetView *rendertarget_view = null;
ID3D11Debug *d3d_debug = null;
ID3D11InfoQueue *info_queue = null;

bool Open()
{
    LOG_Verbose("D3D Open begins");

    DXGI_SWAP_CHAIN_DESC swapchain_desc{ 0 };
    swapchain_desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    swapchain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapchain_desc.BufferCount = params.triple_buffered ? 3 : 2;
    swapchain_desc.SampleDesc.Count = 1;
    swapchain_desc.OutputWindow = hWnd;
    swapchain_desc.SwapEffect = params.swap_effect;
    swapchain_desc.Windowed = !params.fullscreen;

    uint32 flags = 0;
    if(params.debug) {
        LOG_Info("Debug flag is set, creating device with DEBUG flag");
        flags |= D3D11_CREATE_DEVICE_DEBUG;    // can't use DEBUG device in Windows Server 2012 though...
    }

    HRESULT hr = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, flags, NULL, 0, D3D11_SDK_VERSION, &swapchain_desc, &swap_chain, &device, NULL, &context);
    if(FAILED(hr)) {
        LOG_Error("D3D11CreateDeviceAndSwapChain FAILED: %08x", hr);
        return false;
    }

    if(params.debug) {
        hr = device->QueryInterface(__uuidof(ID3D11Debug), (void **)&d3d_debug);
        if(FAILED(hr)) {
            LOG_Error("Error creating d3d_debug object: %08x", hr);
            return false;
        }
        hr = d3d_debug->QueryInterface(__uuidof(ID3D11InfoQueue), (void **)&info_queue);
        if(FAILED(hr)) {
            LOG_Error("Error getting D3D::info_queue: %08x", hr);
            return false;
        }

        info_queue->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_CORRUPTION, true);
        info_queue->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_ERROR, true);

        D3D11_MESSAGE_ID hide[] = {
            D3D11_MESSAGE_ID_SETPRIVATEDATA_CHANGINGPARAMS,    // Add more message IDs here as needed
        };

        D3D11_INFO_QUEUE_FILTER filter;
        memset(&filter, 0, sizeof(filter));
        filter.DenyList.NumIDs = _countof(hide);
        filter.DenyList.pIDList = hide;
        info_queue->AddStorageFilterEntries(&filter);
    }

    hr = swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void **)&backbuffer);
    if(FAILED(hr)) {
        LOG_Error("swap_chain->GetBuffer FAILED: %08x", hr);
        return false;
    }
    hr = device->CreateRenderTargetView(backbuffer, NULL, &rendertarget_view);
    if(FAILED(hr)) {
        LOG_Error("CreateRenderTargetView FAILED: %08x", hr);
        return false;
    }
    backbuffer->Release();

    LOG_Debug("D3D Open complete");
    return true;
}

//////////////////////////////////////////////////////////////////////

void Close()
{
    LOG_Debug("D3D Close");

    swap_chain->SetFullscreenState(false, null);

    if(context != null) {
        context->ClearState();
    }
    context->Flush();

    COM_Release(rendertarget_view);
    COM_Release(context);
    COM_Release(swap_chain);
    COM_Release(info_queue);
    COM_Release(d3d_debug);
    COM_Release(device);
}

};    // namespace D3D

//////////////////////////////////////////////////////////////////////
// Press pad button A or key A to flash screen white

void Render()
{
    XINPUT_STATE pad_state{ 0 };
    if(XInput::GetState(0, &pad_state) != ERROR_SUCCESS) {
        static bool controller_warned = false;
        if(!controller_warned) {
            LOG_Error("No xbox controller in slot 0");
            controller_warned = true;
        }
    }
    float *color = blank_color;
    if(pad_state.Gamepad.wButtons & params.flash_buttons || GetAsyncKeyState('X') & 0x8000) {
        color = flash_color;
    }
    D3D::context->ClearRenderTargetView(D3D::rendertarget_view, color);
    D3D::swap_chain->Present(params.vsync_frames, 0);

    if(params.debug) {
        LOG_Context("D3D");
        uint64 n = D3D::info_queue->GetNumStoredMessages();
        std::vector<char> message_data;
        for(uint64 i = 0; i < n; ++i) {
            SIZE_T message_len;
            HRESULT hr = D3D::info_queue->GetMessage(n, NULL, &message_len);
            D3D11_MESSAGE *p = (D3D11_MESSAGE *)new byte[message_len];
            defer(delete[] p);
            hr = D3D::info_queue->GetMessage(n, p, &message_len);
            LOG_Info("%-*s", p->DescriptionByteLength, p->pDescription);
        }
        D3D::info_queue->ClearStoredMessages();
    }
}

//////////////////////////////////////////////////////////////////////

bool window_size_from_string(std::string const &s, int &width, int &height)
{
    std::vector<std::string> parts;
    tokenize(s, parts, "x", tokenize_option::discard_empty);
    if(parts.size() != 2) {
        return false;
    }
    int w = atoi(parts[0].c_str());
    int h = atoi(parts[1].c_str());
    if(w < 160 || w > 3840) {
        LOG_Error("Width specified (%d) out of range, must be 160 < W < 3840", w);
        return false;
    }
    if(h < 120 || w > 2160) {
        LOG_Error("Height specified (%d) out of range, must be 120 < W < 2160", h);
        return false;
    }
    width = w;
    height = h;
    return true;
}

//////////////////////////////////////////////////////////////////////

std::map<std::string, uint32> swap_effect_names = { { "DISCARD", DXGI_SWAP_EFFECT_DISCARD },
                                                    { "SEQUENTIAL", DXGI_SWAP_EFFECT_SEQUENTIAL },
                                                    { "FLIP_SEQUENTIAL", DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL },
                                                    { "FLIP_DISCARD", DXGI_SWAP_EFFECT_FLIP_DISCARD } };

//////////////////////////////////////////////////////////////////////

std::map<char, uint32> button_names = {
    { 'A', XINPUT_GAMEPAD_A },
    { 'B', XINPUT_GAMEPAD_B },
    { 'X', XINPUT_GAMEPAD_X },
    { 'Y', XINPUT_GAMEPAD_Y },
};

//////////////////////////////////////////////////////////////////////

bool button_mask_from_string(std::string const &s, uint32 &result)
{
    std::string u = str_to_upper(s);
    uint32 mask = 0;
    for(char const c : u) {
        uint32 button;
        if(!map_find(button_names, c, button)) {
            LOG_Error("Unknown button '%c'", c);
            return false;
        }
        mask |= button;
    }
    if(mask == 0) {
        LOG_Error("Button mask is empty!");
        return false;
    }
    result = mask;
    return true;
}

//////////////////////////////////////////////////////////////////////

std::string string_from_button_mask(uint32 mask)
{
    std::string r("----");
    int d = 0;
    for(auto const &kvp : button_names) {
        if((mask & kvp.second) != 0) {
            r[d] = kvp.first;
        }
        d += 1;
    }
    return r;
}

//////////////////////////////////////////////////////////////////////
// nab the command line args
// it doesn't complain about unrecognized arguments
// it doesn't show a usage message

void process_arguments(LPWSTR lpCmdLine)
{
    LOG_Context("Args");

    wchar const *env_var_name = L"dx11_blank_command_line";
    char const *env_var_nameA = "dx11_blank_command_line";

    std::wstring command_line(lpCmdLine);

    // if config.ini exists and has a command line in it, get it from there...
    std::ifstream f("config.ini", std::ifstream::in);
    std::string line;

    while(std::getline(f, line)) {
        if(str_icompare(line, env_var_nameA)) {
            size_t o = line.find('=', 0);
            if(o != std::string::npos) {
                o += 1;
                size_t len = line.size() - o;
                if(line[o] == '"') {
                    o += 1;
                    len -= 1;
                }
                command_line = stringToWide(CP_ACP, line.substr(o, len).c_str());
                break;
            }
        }
    }

    // NOT THE ENV VAR THING, USE CONFIG.INI SO YOU
    // CAN CREATE TAILORED CLIENTS IN THE DASHBOARD

    // // maybe get it from the env var
    // std::vector<wchar> buffer;
    // uint32 len = GetEnvironmentVariableW(env_var_name, null, 0);
    // if(len != 0 && GetLastError() != ERROR_ENVVAR_NOT_FOUND) {
    //    buffer.resize(len);
    //    uint32 got = GetEnvironmentVariableW(env_var_name, buffer.data(), len);
    //    if(got == len - 1) {
    //        command_line = buffer.data();
    //    }
    //}

    int argc;
    LPWSTR *s = CommandLineToArgvW(command_line.c_str(), &argc);

    // convert args to utf8
    std::vector<std::string> utf8_arg_strings;
    std::vector<char const *> utf8_argv;
    for(int i = 0; i < argc; ++i) {
        utf8_arg_strings.push_back(wideToString(CP_UTF8, s[i]));
    }
    for(int i = 0; i < argc; ++i) {
        utf8_argv.push_back(utf8_arg_strings[i].c_str());
    }

    // parse into arguments
    argh::parser args({ "swap_effect", "vsync_frames", "flash_buttons", "blank_color", "flash_color", "window_size", "log_level" });
    args.parse(argc, utf8_argv.data());

    // log level first!
    args("log_level") >> params.log_level;
    Log_SetLevel(params.log_level);

    if(params.log_level > 0) {
        Log_SetOutputFiles(LoggerAdmin::log_filename());
    }

    LOG_Debug("Log level set to %d (ended up as %d)", params.log_level, Logging::log_level);

    // process the arguments
    params.fullscreen = args["fullscreen"];    // square brackets for flags
    params.triple_buffered = args["triple_buffered"];
    params.debug = args["debug"];

    args("triple_buffered") >> params.triple_buffered;    // application operator returns a std::istringstream which will be empty/bad if
    args("fullscreen") >> params.fullscreen;              // the param is not specified so the target of the shift operator will be unmodified

    auto swap_effect_arg = args("swap_effect");    // bool operator on a stream returns false if it's empty/bad
    auto vsync_arg = args("vsync");
    auto flash_buttons_arg = args("flash_buttons");
    auto blank_color_arg = args("blank_color");
    auto flash_color_arg = args("flash_color");
    auto window_size_arg = args("window_size");

    if(swap_effect_arg) {
        if(!map_find(swap_effect_names, str_to_upper(swap_effect_arg.str()), params.swap_effect)) {
            auto s = map_reverse_find(swap_effect_names, params.swap_effect);
            LOG_Error("Bad swap effect \"%s\", defaulting to %s", swap_effect_arg.str().c_str(), s.c_str());
        }
    }

    if(vsync_arg) {
        vsync_arg >> params.vsync_frames;    // shifting an istringstream into an int does an atoi I guess
        if(params.vsync_frames < 0 || params.vsync_frames > 60) {
            LOG_Warn("Warning, %d is not a good value for vsync_frames, defaulting to 1", params.vsync_frames);
            params.vsync_frames = 1;
        }
    }

    if(flash_buttons_arg) {
        if(!button_mask_from_string(flash_buttons_arg.str(), params.flash_buttons)) {
            LOG_Error("Bad button mask \"%s\", defaulting to ABXY", flash_buttons_arg.str().c_str());
        }
    }

    if(blank_color_arg) {
        if(!rgb24_from_string(blank_color_arg.str(), params.blank_color)) {
            LOG_Warn("Bad color \"%s\" for %s, defaulting to %06x", "blank_color", blank_color_arg.str(), params.blank_color);
        }
    }

    if(flash_color_arg) {
        if(!rgb24_from_string(flash_color_arg.str(), params.flash_color)) {
            LOG_Warn("Bad color \"%s\" for %s, defaulting to %06x", "flash_color", flash_color_arg.str(), params.flash_color);
        }
    }

    if(window_size_arg) {
        if(!window_size_from_string(window_size_arg.str(), params.window_width, params.window_height)) {
            LOG_Warn("Bad argument for \"%s\", defaulting to %dx%d", "window_size", params.window_width, params.window_height);
        }
    }

    LOG_Verbose("Fullscreen = %s", params.fullscreen ? "true" : "false");
    LOG_Verbose("TripleBuffered = %s", params.triple_buffered ? "true" : "false");
    LOG_Verbose("DebugMode = %s", params.debug ? "true" : "false");
    LOG_Verbose("Swap Effect is %s", map_reverse_find(swap_effect_names, params.swap_effect).c_str());
    LOG_Verbose("VSync = %d", params.vsync_frames);
    LOG_Verbose("FlashButtons = %s", string_from_button_mask(params.flash_buttons).c_str());
    LOG_Verbose("BlankColor = %06x", params.blank_color);
    LOG_Verbose("FlashColor = %06x", params.flash_color);
    LOG_Verbose("Window Size = %dx%d", params.window_width, params.window_height);

    rgb24_to_floats(params.flash_color, flash_color);
    rgb24_to_floats(params.blank_color, blank_color);
}

};    // namespace

//////////////////////////////////////////////////////////////////////

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
    LOG_Context("WinMain");

    process_arguments(lpCmdLine);

    LOG_Info("Starting");

    if(!InitWindow(hInstance)) {
        return 1;
    }

    if(!D3D::Open()) {
        return 2;
    }

    ShowWindow(hWnd, SW_SHOW);
    MSG msg{ 0 };
    while(msg.message != WM_QUIT) {
        if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != 0) {
            DispatchMessage(&msg);
        } else {
            Render();
        }
    }

    D3D::Close();
    XInput::cleanup();

    LOG_Info("Ending");
    return 0;
}
