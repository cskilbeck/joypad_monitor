#pragma once

#include "util.h"

void random_init(uint64_t seed);
uint32_t random();

namespace rnd {

void init();
void seed(uint64 seed);
uint32 next();

}    // namespace rand
