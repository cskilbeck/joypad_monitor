//////////////////////////////////////////////////////////////////////
// TODO (chs): save/load config vars to flash
// TODO (chs): reset command

// DONE (chs): trigger auto on a joypad button
// DONE (chs): make auto command use config vars
// DONE (chs): set command
// DONE (chs): split this file up (kinda, it'll do for now)

// NOPE (chs): check murmur hash didn't get broken in translation (use eeprom lib instead)

//////////////////////////////////////////////////////////////////////
// Putty settings
//
// Implicit CR in every LF          ON
// Implicit LF in every CR          OFF
// Local Echo                       Force on
// Local line editing               Force on
//
// if you want local line editing
//
// OR
//
// Implicit CR in every LF          ON
// Implicit LF in every CR          ON
// Local Echo                       Force on
// Local line editing               Force off
//
// if you want CTRL-C handler to be called immediately, instead of after you press return
//
// OR forget Putty and write an app on the PC with a GUI

#include "util.h"
#include "main.h"
#include "ansi.h"
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_pcd.h"
#include "stm32f1xx_it.h"
#include "usb_device.h"
#include "printf.h"
#include "usbd_cdc_if.h"
#include "rand.h"
#include "command_line.h"
#include "output_buffer.h"
#include "process.h"
#include "config_var.h"
#include "timer.h"
#include "flash.h"

//////////////////////////////////////////////////////////////////////

static void clock_init(void);
static void gpio_init(void);
static void timer_init(void);

//////////////////////////////////////////////////////////////////////

// these are both incremented at 10KHz
volatile uint32 timer_ticks = 0;
volatile uint32_t process_ticks = 0;

// this is incremented each time TIM2 wraps (period is 0xFFFF, full speed) which gives a 48 bit 72MHz timer (1 tick = ~0.13 uS)
volatile uint32 timer2 = 0;

// if this function pointer is not null, it will be called at 10 KHz
// it's not chainable, if you want some fancy delegate chain thing, do it yourself
timer_callback_t timer_callback = null;

// USB is or isn't connected
bool connected = false;

// # of lines of input from the usb comport so far
int usb_lines_received = 0;

// double buffer command lines so new one can arrive while this one being processed
command_line command_lines[2];
int current_command_line = 0;
command_line *volatile current_command = null;

// timer admin
TIM_HandleTypeDef htim1;    // 10KHz timer for timing latencies
TIM_HandleTypeDef htim2;    // high resolution timer, just for entropy for now

//////////////////////////////////////////////////////////////////////
// printf admin

int current_output_buffer = 0;

struct usb_output_buffer : output_buffer_t<1024>
{
    virtual void flush() override
    {
        if(connected && !empty()) {
            bool sent = false;
            while(!sent) {
                switch(CDC_Transmit_FS(buffer, len)) {
                case USBD_OK:
                    sent = true;
                    break;
                case USBD_FAIL:
                    DEBUG_LED_ON;
                    sent = true;
                    break;
                case USBD_BUSY:
                    break;
                }
            }
        }
        current_output_buffer = 1 - current_output_buffer;
        reset();
    }
};

// should really use a ring buffer for outgoing usb traffic but this will do for now
usb_output_buffer output_buffers[2];

//////////////////////////////////////////////////////////////////////
// they called set_line_coding with valid numbers (eg putty connected)

void on_connected()
{
    if(!connected) {
        connected = true;
        usb_lines_received = 0;
    }
}

//////////////////////////////////////////////////////////////////////
// got a ctrl-c from the usb comport

void on_break()
{
    process::current_process->on_break();
}

//////////////////////////////////////////////////////////////////////
// got a line of text from the usb comport

void on_line_received(uint8_t *line, uint32 len)
{
    if(usb_lines_received == 0) {
        printf(ansi_yellow "Welcome to the Joypad Monitor!\n" ansi_cyan "Type 'help' to see a list of commands\n" ansi_off);
        config_var::load_all();
    }

    usb_lines_received += 1;

    command_line &c = command_lines[current_command_line];
    c.process_args((char *)line, len);    // copy it and get argc, argv

    current_command = &c;    // set this to cause the main loop to dispatch it

    current_command_line = 1 - current_command_line;    //
}

//////////////////////////////////////////////////////////////////////
// printf calls this to output a character

void _putchar(char character)
{
    output_buffers[current_output_buffer].add_char(character);
}

//////////////////////////////////////////////////////////////////////

int main()
{
    HAL_Init();
    clock_init();
    gpio_init();
    timer_init();
    usb_init();

    rnd::init();

    process::current_process = new(process::process_object) cli_process();

    while(1) {

        // handle
        if(current_command != null) {
            int argc = current_command->argc;
            char **argv = current_command->argv;
            current_command = null;    // technical race here with the isr but in practice not a problem
            process::current_process->on_command(argc, argv);
        }

        // current process loop
        process::current_process->update();

        // flush any printf traffic so it is seen asap
        output_buffers[current_output_buffer].flush();
    }
}

//////////////////////////////////////////////////////////////////////

static void clock_init(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct;
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_PeriphCLKInitTypeDef PeriphClkInit;
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
    HAL_RCC_OscConfig(&RCC_OscInitStruct);
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
    HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
    PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
    HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);
    HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);
    HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
    HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

//////////////////////////////////////////////////////////////////////

static void timer_init(void)
{
    htim1.Instance = TIM1;
    htim1.Init.Prescaler = 0;
    htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim1.Init.Period = 7199;    // 10KHz
    htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim1.Init.RepetitionCounter = 0;
    htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
    HAL_TIM_Base_Init(&htim1);

    htim2.Instance = TIM2;
    htim2.Init.Prescaler = 0;
    htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim2.Init.Period = 0xffff;
    htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim2.Init.RepetitionCounter = 0;
    htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
    HAL_TIM_Base_Init(&htim2);

    TIM_ClockConfigTypeDef sClockSourceConfig;
    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig);
    HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig);

    TIM_MasterConfigTypeDef sMasterConfig;
    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig);
    HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig);

    HAL_TIM_Base_Start_IT(&htim1);
    HAL_TIM_Base_Start_IT(&htim2);
}

//////////////////////////////////////////////////////////////////////

static void gpio_init(void)
{
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();

    GPIO_InitTypeDef g;
    g.Speed = GPIO_SPEED_FREQ_HIGH;
    g.Pull = GPIO_NOPULL;

    // debug led and decimal point
    g.Pin = LED_DBG1_Pin | DP2_Pin;
    g.Mode = GPIO_MODE_OUTPUT_PP;
    HAL_GPIO_Init(GPIOC, &g);

    // light sensor input
    g.Pin = IN_SNS_Pin;
    g.Mode = GPIO_MODE_INPUT;
    HAL_GPIO_Init(GPIOC, &g);

    // joypad buttons
    g.Pin = PAD_A_Pin | PAD_B_Pin | PAD_X_Pin | PAD_Y_Pin;
    g.Mode = GPIO_MODE_INPUT;
    HAL_GPIO_Init(GPIOA, &g);

    // leds
    g.Pin = LED_A_Pin | LED_B_Pin | LED_X_Pin | LED_Y_Pin | LED_SNS_Pin;
    g.Mode = GPIO_MODE_OUTPUT_PP;
    HAL_GPIO_Init(GPIOA, &g);

    // the digits, 4 bits each
    g.Pin = 0xffff;
    g.Mode = GPIO_MODE_OUTPUT_PP;
    HAL_GPIO_Init(GPIOB, &g);
}

//////////////////////////////////////////////////////////////////////
