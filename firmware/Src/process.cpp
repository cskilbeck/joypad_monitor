//////////////////////////////////////////////////////////////////////

#include "stm32f1xx_hal.h"
#include "util.h"
#include "rand.h"
#include "printf.h"
#include "ansi.h"
#include "timer.h"
#include "process.h"
#include "command.h"
#include "config_var.h"

//////////////////////////////////////////////////////////////////////

namespace process {

byte process_object[64];
process_base *current_process = null;
int counter = 0;

}    // namespace process

//////////////////////////////////////////////////////////////////////
// convert an integer from 0..9999 to 4 bcd digit nibbles

namespace {

uint32 get_digits(uint32 val)
{
    uint32 digits = 0;
    for(uint32 i = 0; i < 16; i += 4) {
        uint32 div, mod;
        divmod(val, 10, div, mod);
        digits |= mod << i;
        val = div;
    }
    return digits;
}

}    // namespace

//////////////////////////////////////////////////////////////////////
// set the timer digits

static void update_time_display(uint32 show_time)
{
    uint32 decimal_point = DP2_Pin << 16;    // decimal point on (1/10ths of a ms)

    if(show_time > 99990) {    // limit to ten seconds max
        show_time = 99990;
    }
    if(show_time > 9999) {       // if >= 1 second
        show_time /= 10;         // show in ms
        decimal_point >>= 16;    // and decimal point off
    }
    DP2_GPIO_Port->BSRR = decimal_point;
    GPIOB->ODR = get_digits(show_time);
}

//////////////////////////////////////////////////////////////////////

process_base::process_base()
{
    process_ticks = 0;    // process_ticks is how long current process has been running
}

//////////////////////////////////////////////////////////////////////

void process_base::end_process()
{
    process::start<cli_process>();
}

//////////////////////////////////////////////////////////////////////

void process_base::prompt()
{
    printf(PROMPT, name());
}

//////////////////////////////////////////////////////////////////////

void cli_process::on_command(int argc, char **argv)
{
    printf(ansi_off);
    int n = process::counter;
    if(argc > 0) {
        command *c = command::find(argv[0]);
        if(c != null) {
            c->handle(argc, argv);
        } else {
            printf("Unknown command: %s\nTry 'help'\n", argv[0]);
        }
    }
    if(n == process::counter) {
        prompt();
    }
}

//////////////////////////////////////////////////////////////////////

void cli_process::on_break()
{
    printf(BREAK);
    printf("\nNothing running to break...\n");
}

//////////////////////////////////////////////////////////////////////

cli_process::cli_process() : process_base()
{
    records = 0;
    display_time = 0;
    old_buttons = 0xf;
    old_light = 0;
    freeze = true;
    separator = "\n";
    trigger_button_var = config_var::find("trigger_buttons");

    // set GPIOA:0..3 to floating input (the pad pulls them up)
    GPIOA->CRL = (GPIOA->CRL & 0xffff0000) | 0x00004444;
}

//////////////////////////////////////////////////////////////////////

char const *cli_process::name()
{
    return "CLI";
}

//////////////////////////////////////////////////////////////////////
// in cli mode return the actual buttons, 4 bits, 1 = pressed

uint32 cli_process::read_buttons()
{
    return ~GPIOA->IDR & 0xf;
}

//////////////////////////////////////////////////////////////////////
// do nothing in cli mode

void cli_process::release_buttons()
{
}

//////////////////////////////////////////////////////////////////////
// screen went black (because button released), do nothing in cli mode

void cli_process::on_went_black()
{
}

//////////////////////////////////////////////////////////////////////

void cli_process::update()
{
    uint32 time_stamp = timer_ticks;

    // read the light sensor
    uint32 light = GPIOC->IDR & IN_SNS_Pin;
    uint32 light_change = light ^ old_light;
    old_light = light;

    bool went_white = light_change && light;
    bool went_black = light_change && !light;

    // if light_change, record latency and freeze the timer until a button is pressed again
    if(went_white && !freeze) {
        printf(ansi_off "%s%d,%d.%d,%d.%d", separator, records, process_ticks / 10, process_ticks % 10, time_stamp / 10, time_stamp % 10);
        separator = "\n";
        records += 1;
        freeze = true;
        release_buttons();
    }

    if(went_black) {
        on_went_black();
    }

    // read the buttons
    uint32 buttons = read_buttons() & trigger_button_var->value;
    ;    // 1 = pressed
    uint32 buttons_pressed = (buttons ^ old_buttons) & buttons;
    old_buttons = buttons;

    // any press resets time to zero and starts it running
    if(buttons_pressed != 0) {
        timer_ticks = 0;
        freeze = false;
    }

    // display time advances while button held
    if(!freeze) {
        display_time = time_stamp;
    }

    // colored LEDs reflect button state and light sensor state
    uint32 button_leds = ((~buttons & 0xf) << 4) | ((buttons & 0xf) << 20);

    // set the middle led based on the light sensor
    uint32 sense_led = LED_SNS_Pin << (light ? 16 : 0);

    GPIOA->BSRR = button_leds | sense_led;

    // timer display
    update_time_display(display_time);
}

//////////////////////////////////////////////////////////////////////

test_process::test_process() : process_base()
{
    shift = 16;
    digits = 0x8888;
    printf("Testing! Ctrl-C to exit\n");
    prompt();
}

//////////////////////////////////////////////////////////////////////

char const *test_process::name()
{
    return "TEST";
}

//////////////////////////////////////////////////////////////////////

void test_process::on_break()
{
    printf(BREAK "Test complete!\n");
    DEBUG_LED_OFF;
    end_process();
}

//////////////////////////////////////////////////////////////////////

void test_process::on_command(int argc, char **argv)
{
    // janky command processor, why are we not using the main
    // command interpreter?
    extern volatile uint32 timer2;
    printf("timer2: %04x:%04x\n", timer2, TIM2->CNT);
    shift = 16 - shift;
    digits += 0x1111;
    if(digits == 0xbbbb) {
        digits = 0;
    }
    for(int i = 0; i < argc; ++i) {
        printf("%d[%s]\n", i, argv[i]);
    }
    prompt();
}

//////////////////////////////////////////////////////////////////////

void test_process::update()
{
    GPIOA->BSRR = uint32(LED_A_Pin | LED_B_Pin | LED_X_Pin | LED_Y_Pin | LED_SNS_Pin) << shift;
    GPIOC->BSRR = uint32(LED_DBG1_Pin | DP2_Pin) << shift;
    GPIOB->ODR = digits;
}

//////////////////////////////////////////////////////////////////////
// inputs
// min delay        wait at least this long before pressing button
// max delay        wait at most this long before pressing button
// record count     take this many samples
// button mask      press these buttons (0000 - 1111) (ABXY)

auto_process::auto_process() : cli_process()
{
    started = false;

    // defaults / config_vars for some things
    config_var *iterations = config_var::find("auto_iterations");
    config_var *min_delay_var = config_var::find("auto_min_delay");
    config_var *max_delay_var = config_var::find("auto_max_delay");
    config_var *auto_buttons_var = config_var::find("auto_buttons");
    config_var *auto_start_buttons_var = config_var::find("auto_start_buttons");
    config_var *trigger_buttons_var = config_var::find("trigger_buttons");

    max_samples = (iterations != null) ? iterations->value : 10;
    min_delay = (min_delay_var != null) ? min_delay_var->value * 10 : 100 * 10;
    max_delay = (max_delay_var != null) ? max_delay_var->value * 10 : 200 * 10;
    auto_buttons = (auto_buttons_var != null) ? auto_buttons_var->value & 0xf : 0xf;

    // first record output resets console, makes copying to excel easier
    separator = CONSOLE_RESET;

    char trigger_str[5];
    char auto_str[5];
    button_mask_to_str(trigger_str, trigger_buttons_var->value);
    button_mask_to_str(auto_str, auto_buttons_var->value);

    printf("auto_buttons: 0x%1x (%s)\ntrigger_buttons: 0x%1x (%s)\n", auto_buttons_var->value, auto_str, trigger_buttons_var->value, trigger_str);

    if((auto_buttons & trigger_buttons_var->value) == 0) {
        printf(ansi_red "Error: auto_buttons will press %s and trigger_buttons is %s\n" ansi_yellow "Press enter to quit:" ansi_off, auto_str, trigger_str);
        end_process();
    } else {
        start_buttons = auto_start_buttons_var->value;
        char start_str[5];
        button_mask_to_str(start_str, start_buttons);
        printf(ansi_yellow "Press joypad %s to start the test!\n" ansi_off, start_str);
    }
}

//////////////////////////////////////////////////////////////////////

void auto_process::update()
{
    // wait for the start_button(s) to be pressed before going into regular update mode
    if(!started) {
        uint32 b = cli_process::read_buttons();
        started = (b & start_buttons) != 0;
        if(started) {
            printf(ansi_green "Here we go..." ansi_off);

            // dodgy random seed
            rnd::seed((uint64)TIM2->CNT | (timer2 << 16));

            delay = get_delay();

            num_samples = 0;

            // set GPIOA outputs[0..3] to one (eg buttons released)
            GPIOA->BSRR = auto_buttons;

            // set GPIOA:0..3 to output push_pull
            GPIOA->CRL = (GPIOA->CRL & 0xffff0000) | 0x00003333;
        }
    }

    if(started) {
        cli_process::update();
    }
}

//////////////////////////////////////////////////////////////////////

int auto_process::get_delay()
{
    int delay_range = max_delay - min_delay;
    return process_ticks + (rnd::next() % delay_range) + min_delay;
}

//////////////////////////////////////////////////////////////////////

char const *auto_process::name()
{
    return "Auto";
}

//////////////////////////////////////////////////////////////////////

void auto_process::on_command(int argc, char **argv)
{
    on_break();
}

//////////////////////////////////////////////////////////////////////

void auto_process::on_break()
{
    printf(BREAK);
    end_process();
}

//////////////////////////////////////////////////////////////////////
// in auto mode, press the buttons after a random delay

uint32 auto_process::read_buttons()
{
    uint32 b = 0;
    if(delay != 0 && process_ticks > delay) {
        b = auto_buttons;
    }
    GPIOA->BSRR = b << 16;
    return b;
}

//////////////////////////////////////////////////////////////////////
// screen gone black after releasing buttons, OK to do another sample

void auto_process::on_went_black()
{
    delay = get_delay();
}

//////////////////////////////////////////////////////////////////////
// in auto mode release buttons when the light_sensor lights up
// and go into `wait for black` mode by setting delay to 0

void auto_process::release_buttons()
{
    delay = 0;                     // freeze delay timer
    GPIOA->BSRR = auto_buttons;    // release the buttons

    num_samples += 1;

    if(num_samples == max_samples) {
        printf("\n");
        end_process();
    }
}
