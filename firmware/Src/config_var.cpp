//////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include "util.h"
#include "ansi.h"
#include "printf.h"
#include "config_var.h"
#include "flash.h"

//////////////////////////////////////////////////////////////////////

namespace {

// for recognizing button names

struct button
{
    char c;
    int mask;
};

const button buttons[4] = { { 'A', 1 }, { 'B', 2 }, { 'X', 4 }, { 'Y', 8 } };

}    // namespace

//////////////////////////////////////////////////////////////////////
// used from command.cpp, declared in util.h, woop de doo

void button_mask_to_str(char *buffer, uint32 button_mask)
{
    for(int i = 0; i < countof(buttons); ++i) {
        buffer[i] = (button_mask & buttons[i].mask) ? buttons[i].c : '-';
    }
    buffer[4] = 0;
}

//////////////////////////////////////////////////////////////////////
// this without error checking...

uint32 str_to_button_mask(char const *str)
{
    uint32 val = 0;
    while(true) {
        char c = toupper(*str++);
        if(c == 0) {
            break;
        }
        bool found = false;
        for(int i = 0; i < countof(buttons); ++i) {
            if(c == buttons[i].c) {
                val |= buttons[i].mask;
                break;
            }
        }
    }
    return val;
}

//////////////////////////////////////////////////////////////////////
// an integer config var

struct int_var : config_var
{
    int_var() : config_var()
    {
    }

    virtual char const *type()
    {
        return "uint";
    }

    virtual bool set(char const *test)
    {
        uint32 v;
        switch(str_to_u(test, v)) {
        case e_ok:
            value = v;
            return true;
        case e_bad_digit:
            printf(ansi_red "bad digit\n" ansi_off);
            break;
        case e_empty_string:
            printf(ansi_red "no number!?\n" ansi_off);
            break;
        case e_overflow:
            printf(ansi_red "overflow\n" ansi_off);
            break;
        }
        return false;
    }

    virtual void print_val(char buffer[16], uint32 val)
    {
        sprintf(buffer, "%u", val);
    }
};

//////////////////////////////////////////////////////////////////////
// a button mask config var

struct button_mask_var : config_var
{
    button_mask_var() : config_var()
    {
    }

    virtual char const *type()
    {
        return "buttons";
    }

    virtual void print_val(char buffer[16], uint32 val)
    {
        button_mask_to_str(buffer, val);
    }

    virtual bool set(char const *test)
    {
        uint32 new_value = 0;
        int matched = 0;
        while(true) {
            char c = toupper(*test++);
            if(c == 0) {
                break;
            }
            bool found = false;
            for(int i = 0; i < countof(buttons); ++i) {
                if(c == buttons[i].c) {
                    if(new_value & buttons[i].mask) {
                        printf(ansi_red "Duplicate button: %c\n" ansi_off, c);
                        return false;
                    }
                    new_value |= buttons[i].mask;
                    found = true;
                    matched += 1;
                    break;
                }
            }
            if(!found) {
                printf(ansi_red "Unknown button: %c, valid buttons are A B X Y\n" ansi_off, c);
                return false;
            }
        }
        if(matched == 0) {
            printf(ansi_red "Huh? no buttons found, which is really odd....\n" ansi_off);
            return false;
        }
        value = new_value;
        return true;
    }
};

//////////////////////////////////////////////////////////////////////
// now the config vars

struct auto_buttons_var : button_mask_var
{
    auto_buttons_var() : button_mask_var()
    {
        // note: calls to factory_default() _cannot_ be pushed down to
        // an inner ctor because the vtable would not be valid yet
        value = factory_default();
    }

    virtual byte id() const
    {
        return 0x01;
    }

    virtual uint32 factory_default() const
    {
        return str_to_button_mask("ABX");
    }

    virtual char const *name()
    {
        return "auto_buttons";
    }

    virtual char const *description()
    {
        return "Which buttons the auto test will press each time";
    }
};

//////////////////////////////////////////////////////////////////////

struct auto_iterations_var : int_var
{
    auto_iterations_var() : int_var()
    {
        value = factory_default();
    }

    virtual byte id() const
    {
        return 0x02;
    }

    virtual uint32 factory_default() const
    {
        return 10;
    }

    virtual char const *name()
    {
        return "auto_iterations";
    }

    virtual char const *description()
    {
        return "How many iterations in the auto test";
    }
};

//////////////////////////////////////////////////////////////////////

struct auto_min_delay_var : int_var
{
    auto_min_delay_var() : int_var()
    {
        value = factory_default();
    }

    virtual byte id() const
    {
        return 0x03;
    }

    virtual uint32 factory_default() const
    {
        return 500;
    }

    virtual char const *name()
    {
        return "auto_min_delay";
    }

    virtual char const *description()
    {
        return "Shortest delay during auto test (in milliseconds)";
    }
};

//////////////////////////////////////////////////////////////////////

struct auto_max_delay_var : int_var
{
    auto_max_delay_var() : int_var()
    {
        value = factory_default();
    }

    virtual byte id() const
    {
        return 0x04;
    }

    virtual uint32 factory_default() const
    {
        return 1000;
    }

    virtual char const *name()
    {
        return "auto_max_delay";
    }

    virtual char const *description()
    {
        return "Longest delay during auto test (in milliseconds)";
    }
};

//////////////////////////////////////////////////////////////////////

struct trigger_buttons_var : button_mask_var
{
    trigger_buttons_var() : button_mask_var()
    {
        value = factory_default();
    }

    virtual byte id() const
    {
        return 0x05;
    }

    virtual uint32 factory_default() const
    {
        return str_to_button_mask("ABX");
    }

    virtual char const *name()
    {
        return "trigger_buttons";
    }

    virtual char const *description()
    {
        return "Which buttons start the timer";
    }
};

//////////////////////////////////////////////////////////////////////

struct auto_start_buttons_var : button_mask_var
{
    auto_start_buttons_var() : button_mask_var()
    {
        value = factory_default();
    }

    virtual byte id() const
    {
        return 0x06;
    }

    virtual uint32 factory_default() const
    {
        return str_to_button_mask("Y");
    }

    virtual char const *name()
    {
        return "auto_start_buttons";
    }

    virtual char const *description()
    {
        return "Which buttons start the auto test";
    }
};

//////////////////////////////////////////////////////////////////////

namespace {

auto_buttons_var auto_buttons_instance;
auto_iterations_var auto_iterations_instance;
auto_min_delay_var auto_min_delay_instance;
auto_max_delay_var auto_max_delay_instance;
trigger_buttons_var trigger_buttons_instance;
auto_start_buttons_var auto_start_buttons_instance;

}    // namespace

config_var *config_var::config_vars[] = { &auto_buttons_instance,   &auto_iterations_instance, &auto_min_delay_instance,
                                          &auto_max_delay_instance, &trigger_buttons_instance, &auto_start_buttons_instance };

int const config_var::num_config_vars = countof(config_var::config_vars);

//////////////////////////////////////////////////////////////////////

config_var *config_var::find(char const *n)
{
    for(int i = 0; i < num_config_vars; ++i) {
        if(stricmp(n, config_vars[i]->name()) == 0) {
            return config_vars[i];
        }
    }
    return null;
}

//////////////////////////////////////////////////////////////////////

void config_var::save_all()
{
    printf("Saving config vars\n");
    flash::unlock();
    for(int i = 0; i < num_config_vars; ++i) {
        config_var &v = *config_vars[i];
        printf("Saving var %04x (%s)\n", v.id(), v.name());
        flash::save(v.id(), sizeof(v.value), reinterpret_cast<byte *>(&v.value));
    }
    flash::lock();
    printf("Done\n");
}

//////////////////////////////////////////////////////////////////////

void config_var::load_all()
{
    printf("Loading config vars\n");
    for(int i = 0; i < num_config_vars; ++i) {
        config_var &v = *config_vars[i];
        //printf("Loading var %02x (%s)\n", v.id(), v.name());
        flash::load(v.id(), sizeof(v.value), reinterpret_cast<byte *>(&v.value));
    }
    printf("Done\n");
}