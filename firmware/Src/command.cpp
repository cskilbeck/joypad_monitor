//////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <string.h>
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_flash.h"
#include "stm32f1xx_hal_flash_ex.h"
#include "util.h"
#include "ansi.h"
#include "rand.h"
#include "printf.h"
#include "command.h"
#include "process.h"
#include "config_var.h"
#include "flash.h"

namespace {

//////////////////////////////////////////////////////////////////////

struct help_command : command
{
    virtual char const *name()
    {
        return "help";
    }

    virtual char const *description()
    {
        return "Show list of commands";
    }

    virtual char const *help()
    {
        return "Type help [command] for extra help on a command";
    }

    virtual void handle(int argc, char **argv);
};

//////////////////////////////////////////////////////////////////////

struct cls_command : command
{
    virtual char const *name()
    {
        return "cls";
    }

    virtual char const *description()
    {
        return "Clear the console";
    }

    virtual char const *help()
    {
        return "It resets the scrollback too...";
    }

    virtual void handle(int argc, char **argv)
    {
        printf(CONSOLE_RESET);
    }
};

//////////////////////////////////////////////////////////////////////

struct test_command : command
{
    virtual char const *name()
    {
        return "test";
    }

    virtual char const *description()
    {
        return "Enter LED test mode";
    }
    virtual char const *help()
    {
        return "For testing if the LEDs are all wired up correctly";
    }

    virtual void handle(int argc, char **argv)
    {
        process::start<test_process>();
    }
};

//////////////////////////////////////////////////////////////////////

struct save_command : command
{
    virtual char const *name()
    {
        return "save";
    }

    virtual char const *description()
    {
        return "Save config to flash storage";
    }

    virtual char const *help()
    {
        return "It saves the config vars";
    }

    virtual void handle(int argc, char **argv)
    {
        config_var::save_all();
    }
};

//////////////////////////////////////////////////////////////////////

struct load_command : command
{
    virtual char const *name()
    {
        return "load";
    }

    virtual char const *description()
    {
        return "Load config from flash storage";
    }

    virtual char const *help()
    {
        return "It loads the config vars (it does this at reset automatically)";
    }

    virtual void handle(int argc, char **argv)
    {
        config_var::load_all();
    }
};

//////////////////////////////////////////////////////////////////////

struct format_command : command
{
    virtual char const *name()
    {
        return "format";
    }

    virtual char const *description()
    {
        return "Format the flash storage";
    }

    virtual char const *help()
    {
        return "It formats the flash storage";
    }

    virtual void handle(int argc, char **argv)
    {
        if(argc == 3 && strcmp(argv[1], "FOR") == 0 && strcmp(argv[2], "SURE") == 0) {
            printf("OK then, you asked for it\n");
            flash::unlock();
            flash::format();
            flash::lock();
        } else {
            printf(ansi_red "Warning! " ansi_yellow "All saved variables will be reset to factory defaults!\n" ansi_cyan "If you really want to format the storage, type in " ansi_green
                            "format FOR SURE" ansi_off "\n");
        }
    }
};

//////////////////////////////////////////////////////////////////////

struct auto_command : command
{
    virtual char const *name()
    {
        return "auto";
    }

    virtual char const *description()
    {
        return "Run an Auto session";
    }

    virtual char const *help()
    {
        return "It presses the button for you! See 'vars' for how to configure it";
    }

    virtual void handle(int argc, char **argv)
    {
        process::start<auto_process>();
    }
};

//////////////////////////////////////////////////////////////////////

struct rand_command : command
{
    virtual char const *name()
    {
        return "rand";
    }

    virtual char const *description()
    {
        return "Print some random numbers";
    }

    virtual char const *help()
    {
        return "Usage: rand [N] - it prints N random numbers, default is 10";
    }

    virtual void handle(int argc, char **argv)
    {
        int n = 10;
        if(argc > 1) {
            n = atoi(argv[1]);
            if(n < 0) {
                n = 0;
            } else if(n > 10000) {
                n = 10000;
            }
        }
        rnd::seed((uint64)TIM2->CNT | (timer2 << 16));
        printf("Here's %d random numbers:\n", n);
        for(int i = 0; i < n; ++i) {
            printf("%u\n", rnd::next());
        }
        printf("Done\n");
    }
};

//////////////////////////////////////////////////////////////////////

struct set_command : command
{
    virtual char const *name()
    {
        return "set";
    }

    virtual char const *description()
    {
        return "Set or show config vars";
    }

    virtual char const *help()
    {
        return ansi_off "Usage:\n" ansi_yellow "set          " ansi_off " list vars\n" ansi_yellow "set var value" ansi_off " set a var to a value";
    }

    virtual void handle(int argc, char **argv)
    {
        if(argc == 1) {
            printf("%-20s%-10s%-12s%s\n", "Name", "Value", "Type", "Description");
            char var_str[16];
            for(int i = 0; i < config_var::num_config_vars; ++i) {
                config_var &v = *config_var::config_vars[i];
                v.print_val(var_str, v.value);
                printf(ansi_yellow "%-20s" ansi_off "%-10s%-12s%s\n", v.name(), var_str, v.type(), v.description());
            }
            return;
        }
        if(argc != 3) {
            printf(ansi_green "%s\n" ansi_off, help());
            return;
        }
        config_var *v = config_var::find(argv[1]);
        if(v == null) {
            printf(ansi_red "Unknown config var %s\n" ansi_off, argv[1]);
            return;
        }

        char old_str[16];
        v->print_val(old_str, v->value);

        bool ok = v->set(argv[2]);

        char new_str[16];
        v->print_val(new_str, v->value);

        if(ok) {
            printf("%s changed from %s to %s\n", v->name(), old_str, new_str);
        } else {
            printf("%s unchanged, still %s\n", v->name(), new_str);
        }
    }
};

help_command help_instance;
cls_command cls_instance;
test_command test_instance;
save_command save_instance;
load_command load_instance;
format_command format_instance;
rand_command rand_instance;
auto_command auto_instance;
set_command set_instance;

command *commands[] = { &help_instance, &cls_instance, &test_instance, &save_instance, &load_instance, &format_instance, &rand_instance, &auto_instance, &set_instance };

int const num_commands = countof(commands);

//////////////////////////////////////////////////////////////////////

void print_help(command *c)
{
    printf("%-12s" ansi_yellow "%-32s\n" ansi_off, c->name(), c->description());
}

//////////////////////////////////////////////////////////////////////

void help_command::handle(int argc, char **argv)
{
    command *c = null;
    if(argc == 2) {
        c = command::find(argv[1]);
        if(c == null) {
            printf(ansi_red "Unknown command: %s\n" ansi_off, argv[1]);
        }
    }

    if(c != null) {
        print_help(c);
        printf("%s\n", c->help());
    } else {
        for(int i = 0; i < num_commands; ++i) {
            print_help(commands[i]);
        }
        printf("%s\n", help());
    }
}

}    // namespace

//////////////////////////////////////////////////////////////////////

command *command::find(char const *name)
{
    for(int i = 0; i < num_commands; ++i) {
        command *c = commands[i];
        if(stricmp(name, c->name()) == 0) {
            return c;
        }
    }
    return null;
}
