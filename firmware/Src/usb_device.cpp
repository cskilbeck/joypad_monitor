#include "usb_device.h"
#include "usbd_core.h"
#include "usbd_desc.h"
#include "usbd_cdc.h"
#include "usbd_cdc_if.h"
#include "util.h"
#include "timer.h"

USBD_HandleTypeDef hUsbDeviceFS;

void usb_init(void)
{
    //////////////////////////////////////////////////////////////////////
    // set D+ and D- both low for at least a few milliseconds
    // this causes windows to disable the com port
    // it gets re-enabled when the device is initialised in MX_USB_DEVICE_Init
    // this apparently only works if you have the right 1.5K pullup to 3v3 on D+

    // set A12, A11 as outputs
    GPIO_InitTypeDef g;
    g.Pin = (1 << 11) | (1 << 12);
    g.Mode = GPIO_MODE_OUTPUT_PP;
    g.Speed = GPIO_SPEED_FREQ_HIGH;
    g.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(GPIOA, &g);

    // set them to low
    GPIOA->BSRR = (1 << (12 + 16)) | (1 << (11 + 16));

    // flicker the debug led 7 times x 2.5ms on/off
    for(int j = 0; j < 7; ++j) {
        DEBUG_LED_TOGGLE;
        int t = process_ticks + 250;
        while(process_ticks < t)
            ;
    }

    USBD_Init(&hUsbDeviceFS, &FS_Desc, DEVICE_FS);
    USBD_RegisterClass(&hUsbDeviceFS, &USBD_CDC);
    USBD_CDC_RegisterInterface(&hUsbDeviceFS, &USBD_Interface_fops_FS);
    USBD_Start(&hUsbDeviceFS);
}
