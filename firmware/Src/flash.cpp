//////////////////////////////////////////////////////////////////////
// eeprom emulation flash functions
// see flash.h for interface

#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_flash.h"
#include "stm32f1xx_hal_flash_ex.h"
#include <string.h>
#include <stdio.h>
#include "util.h"
#include "flash.h"
#include "printf.h"

//////////////////////////////////////////////////////////////////////
// notes
// should work on most 32 bit STM32 medium density devices, only tested on STM32F103C8T6
// it's all synchronous
// error checking is very basic, it just aborts the operation if it sees an error
// #define LOG_FLASH to see lots of printf about how it's going
//
// define these 2 variables to set the area used for storage
static int const page_size = FLASH_PAGE_SIZE;                     // usually 1K or 2K
static uint16_t *page_base = (uint16_t *)(FLASH_BASE + 65536);    // this puts your 2 pages at 64k into the flash
//
// define these functions to interface with the hardware (see versions for STM32F1xB medium density at the end of this file)
//
// write a 16 bit word to a flash address
int flash_write_16(uint16_t *dest, uint16_t src);
//
// erase page 0 or page 1 of the flash
int flash_erase_page(int which_page);
//
// wait for the most recent flash operation to complete
void flash_wait();
//
// check whether the most recent flash operation succeded
int flash_result();
//
// they should return one of the status codes defined in flash.h
//
//////////////////////////////////////////////////////////////////////
// method:
//
// 2 pages of flash are used, one is 'active' at a time (except for a brief moment during migration)
//
// the first uint16_t in a flash page contains `page_state`
// valid `page_state` can be
//      "empty"  = 0xFFFF (because erasing the flash sets it to that value
//      "active" = 0x5555 (arbitrarily chosen)
//
// the first uint16_t in a page should be one of these values
// if it's not, it will erase that page to empty
//
// a 2 byte header precedes each stored variable in the flash
//      byte 0 is the id, which can be any value except 0xFF
//      byte 1 is the length, in bytes, of the variable (excluding the var header)
//
// to store a variable, just stash it at the first available free slot
//
// to look up a variable with id X, find the active page, scan for the most recent version of
// the variable with id X (which will the the last one found)
//
// when a page doesn't have room to store the new version of a variable, the page
// is migrated (garbage collected) into the other page, discarding old versions of
// variables and the old page is erased

//////////////////////////////////////////////////////////////////////

#if defined(LOG_FLASH)
#define log printf
#else
void dummy(...)
{
}
#define log     \
    if(false) { \
    } else      \
        dummy
#endif

//////////////////////////////////////////////////////////////////////
// call a flash function and bomb if it doesn't go well

#define check(x)                  \
    {                             \
        int r = (x);              \
        if(r != flash::ok) {      \
            log("Error %d\n", r); \
            return r;             \
        }                         \
    }

//////////////////////////////////////////////////////////////////////

namespace {

//////////////////////////////////////////////////////////////////////

char const *flash_error_strings[] = { "ok", "bad_id", "not_found", "bad_len", "write_error", "cant_unlock", "locked" };

//////////////////////////////////////////////////////////////////////

enum page_state { active = 0x5555, empty = 0xffff };

int const page_size_in_uint16_ts = page_size / sizeof(uint16_t);

//////////////////////////////////////////////////////////////////////
// round a length up to a multiple of 2

int roundup(int len)
{
    return (len + 1) & -2;
}

//////////////////////////////////////////////////////////////////////
// get the id byte of a variable

int var_id(uint16_t *p)
{
    return reinterpret_cast<byte *>(p)[0];
}

//////////////////////////////////////////////////////////////////////
// get the length byte of a variable

int var_len(uint16_t *p)
{
    return reinterpret_cast<byte *>(p)[1];
}

//////////////////////////////////////////////////////////////////////
// get the total # of bytes of a variable including 2 byte header

int total_length(uint16_t *var)
{
    return 2 + roundup(var_len(var));
}

//////////////////////////////////////////////////////////////////////
// get a pointer to page 0 or 1

uint16_t *page_addr(int n)
{
    return page_base + n * page_size_in_uint16_ts;
}

//////////////////////////////////////////////////////////////////////
// get pointer to the data in the page (skip the uint16_t page header)

uint16_t *page_data(int n)
{
    return page_addr(n) + 1;
}

//////////////////////////////////////////////////////////////////////
// get a pointer to just past the end of page 0 or 1

uint16_t *page_end_addr(int n)
{
    return page_addr(n) + page_size_in_uint16_ts;
}

//////////////////////////////////////////////////////////////////////
// get the state of a page, should be `active` or `empty`
// if it's neither of those, the page should be erased

uint16_t get_page_state(int n)
{
    return page_addr(n)[0];
}

//////////////////////////////////////////////////////////////////////
// is a page the active page? page_state::active (0x5555) marks the active page

bool is_active(int which_page)
{
    return get_page_state(which_page) == page_state::active;
}

//////////////////////////////////////////////////////////////////////
// is a page empty? page_state::empty (0xffff) means it's empty (or should be)

bool is_empty(int which_page)
{
    return get_page_state(which_page) == page_state::empty;
}

//////////////////////////////////////////////////////////////////////
// return pointer to the next var

uint16_t *next(uint16_t *cur)
{
    return cur + total_length(cur) / sizeof(uint16_t);
}

//////////////////////////////////////////////////////////////////////
// check if an area is blank

int blank_check(uint16_t *p, uint16_t *e)
{
    while(p < e) {
        if(*p++ != 0xffff) {    // found something other than 0xffff, page is corrupt
            return flash::not_blank;
        }
    }
    return flash::ok;
}

//////////////////////////////////////////////////////////////////////
// get pointer to 1st free slot in a page
// returns null if it's full
// it assumes the page is active

uint16_t *get_free_offset(int which_page)
{
    uint16_t *p = page_data(which_page);
    uint16_t *page_end = page_end_addr(which_page);

    // assert(*p == page_state::active);

    while(p < page_end) {
        if(var_id(p) == 0xff) {
            return p;
        }
        p = next(p);
    }
    return null;
}

//////////////////////////////////////////////////////////////////////
// find a variable in a page
// returns null if it's not found

uint16_t *find_var(int page, int id)
{
    uint16_t *p = page_data(page);
    uint16_t *page_end = page_end_addr(page);
    uint16_t *var = null;
    while(p < page_end) {
        int this_id = var_id(p);
        if(this_id == 0xff) {
            break;
        }
        if(this_id == id) {
            var = p;    // last one found is the most current one
        }
        p = next(p);
    }
    return var;
}

//////////////////////////////////////////////////////////////////////
// write a var to flash
// pointer is incremented to point at next available address (which may be beyond the page end)

int flash_write_var(uint16_t *&p, uint16_t id, uint16_t len, byte *data)
{
    // next available address
    uint16_t *next_space = p + (roundup(len) + 2) / 2;

    uint16_t header = id | (len << 8);
    check(flash_write_16(p++, header));    // write the header

    // write data as uint16_ts
    uint16_t *s = reinterpret_cast<uint16_t *>(data);
    while(len > 1) {
        check(flash_write_16(p++, *s++));
        len -= 2;
    }
    // and last byte for odd length ones
    if(len == 1) {
        check(flash_write_16(p, reinterpret_cast<byte *>(s)[0]));    // so we don't read off the end of the array which would probably be fine but you know how people can get
    }
    p = next_space;
    return flash::ok;
}

//////////////////////////////////////////////////////////////////////
// mark a page as active
// assumes page is empty

int mark_page_active(int page)
{
    log("Marking page %d as active\n", page);
    check(flash_write_16(page_addr(page), page_state::active));
    return flash::ok;
}

//////////////////////////////////////////////////////////////////////
// migrate all the vars (and a new var) from one page to the other
// assumes other_page is freshly erased

int migrate_page(uint16_t id, int old_page, uint16_t len, byte *data)
{
    int new_page = 1 - old_page;

    log("Flash migrating to page %d\n", new_page);

    check(flash_erase_page(new_page));    // erase the other page

    // first save the new var to the new page
    uint16_t *dst_base = page_data(new_page);
    check(flash_write_var(dst_base, id, len, data));

    // scan the old vars
    uint16_t *scan_base = page_data(old_page);

    // for each var in the old page that isn't already in the new page, write latest version to new page
    // inefficient but simple

    while(var_id(scan_base) != 0xff) {
        uint16_t *f = find_var(new_page, var_id(scan_base));    // already written to the new page?
        if(f == null) {
            uint16_t *n = scan_base;    // no, scan for latest version
            uint16_t *scan_var = scan_base;
            while(var_id(scan_var) != 0xff) {
                if(var_id(scan_var) == var_id(scan_base)) {
                    n = scan_var;
                }
                scan_var = next(scan_var);
            }
            check(flash_write_var(dst_base, var_id(n), var_len(n), (byte *)n + 1));    // and copy it
        }
        scan_base = next(scan_base);
    }
    log("Flash migration complete\n");
    check(mark_page_active(new_page));    // finally mark it as active. Now both pages are marked as active (but should have effectively the same contents)
    check(flash_erase_page(old_page));    // erase old page, now just one active page and all is ok
    return flash::ok;
}

//////////////////////////////////////////////////////////////////////
// write a variable to a page
// this might migrate the pages which can take a long time

int write_var(int page, uint16_t id, uint16_t len, byte *data)
{
    uint16_t *loc = get_free_offset(page);                                                       // find some free space at the end
    int remaining_bytes = (loc != null) ? (page_end_addr(page) - loc) * sizeof(uint16_t) : 0;    // work out remaining space if it's not full
    if(remaining_bytes < len) {                                                                  // enough space in this page for this variable?
        check(migrate_page(id, page, len, data));                                                // copy all old values and this new value to the other page
        return flash::ok;
    } else {
        log("Writing %02x (len %d) at offset %d\n", id, len, (loc - page_addr(page)) * 2);
        check(flash_write_var(loc, id, len, data));    // there's enough space, just write the var
        return flash::ok;
    }
}

//////////////////////////////////////////////////////////////////////
// check integrity of a page, erase it if necessary
// this is not at all exhaustive!

int verify_page(int page)
{
    uint16_t *p = page_data(page);
    uint16_t *e = page_end_addr(page);
    if(is_active(page)) {
        p = get_free_offset(page);
    }
    if(blank_check(p, e) != flash::ok) {
        log("Flash page %d corrupt, erasing it\n", page);
        check(flash_erase_page(page));
    }
    return flash::ok;
}

}    // namespace

//////////////////////////////////////////////////////////////////////

namespace flash {

//////////////////////////////////////////////////////////////////////
// call this to check the flash is in a good state
// if it's bad in some way it will try to make it good
// by erasing any pages in a bad state

int verify()
{
    log("Checking flash storage\n");
    verify_page(0);
    verify_page(1);
    return flash::ok;
}

//////////////////////////////////////////////////////////////////////
// reformat the whole thing back to a known good state, losing everything

int format()
{
    log("Formatting flash storage\n");

    check(flash_erase_page(0));
    check(flash_erase_page(1));
    check(mark_page_active(0));
    return flash::ok;
}

//////////////////////////////////////////////////////////////////////
// load a variable from the flash
// if the stored length doesn't match, it doesn't

int load(byte id, byte len, byte *data)
{
    if(id == 0xff) {
        log("Can't use 0xff as a flash variable ID\n");
        return flash::bad_id;
    }
    uint16_t *v = null;
    for(int i = 0; i < 2; ++i) {
        if(is_active(i)) {
            v = find_var(i, id);
            if(v != null) {
                if(var_len(v) != len) {
                    printf("Wrong length, not loaded\n");
                    return flash::bad_len;
                }
                byte *src = reinterpret_cast<byte *>(v + 1);
                memcpy(data, src, len);    // flash is memory mapped
                return flash::ok;
            }
        }
    }
    printf("Var id %d not found\n", id);
    return flash::not_found;
}

//////////////////////////////////////////////////////////////////////

int save(byte id, byte len, byte *data)
{
    log("Saving var %02x (%d bytes) to flash\n", id, len);

    if(id == 0xff) {
        return flash::bad_id;
    }
    for(int p = 0; p < 2; ++p) {
        if(is_active(p)) {
            check(write_var(p, id, len, data));
            return flash::ok;
        }
    }
    for(int p = 0; p < 2; ++p) {
        if(is_empty(p)) {
            check(write_var(p, id, len, data));
            check(mark_page_active(p));
            return flash::ok;
        }
    }
    check(format());
    check(write_var(0, id, len, data));
    return flash::ok;
}

//////////////////////////////////////////////////////////////////////

void lock()
{
    HAL_FLASH_Lock();
}

//////////////////////////////////////////////////////////////////////

int unlock()
{
    if(HAL_FLASH_Unlock() != HAL_OK) {
        return flash::cant_unlock;
    }
    return flash::ok;
}

//////////////////////////////////////////////////////////////////////

char const *error_message(int code)
{
    if(code < 0 || code >= countof(flash_error_strings)) {
        return "not a flash error";
    }
    return flash_error_strings[code];
}

}    // namespace flash



//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
// USER FUNCTIONS
//
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// wait for a flash operation to complete

void flash_wait()
{
    while((FLASH->SR & FLASH_SR_BSY) != 0) {
    }
}

//////////////////////////////////////////////////////////////////////
// check whether a flash operation succeded

int flash_result()
{
    flash_wait();

    if((FLASH->SR & FLASH_SR_WRPRTERR) != 0) {
        return flash::locked;
    }
    if((FLASH->SR & FLASH_SR_PGERR) != 0) {
        return flash::write_error;
    }
    return flash::ok;
}

//////////////////////////////////////////////////////////////////////
// write a 16 bit word to flash

int flash_write_16(uint16_t *dest, uint16_t src)
{
    flash_wait();
    SET_BIT(FLASH->CR, FLASH_CR_PG);
    *(__IO uint16_t *)dest = src;
    return flash_result();
}

//////////////////////////////////////////////////////////////////////
// erase page 0 or 1

int flash_erase_page(int which_page)
{
    // assert(which_page >= 0 && which_page <= 1);
    uint32_t base = (uint32_t)page_addr(which_page);
    log("Erasing flash page %d (%p)\n", which_page, base);
    flash_wait();
    SET_BIT(FLASH->CR, FLASH_CR_PER);
    WRITE_REG(FLASH->AR, base);
    SET_BIT(FLASH->CR, FLASH_CR_STRT);
    check(flash_result());
    check(blank_check((uint16_t *)base, page_end_addr(which_page)));
    return flash::ok;
}
