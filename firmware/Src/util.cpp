//////////////////////////////////////////////////////////////////////

#include "util.h"

//////////////////////////////////////////////////////////////////////
// convert some text as decimal base 10 to uint32
//
// returns one of:
//
//     e_ok = 0
//     e_empty_string = 1
//     e_overflow = 2
//     e_bad_digit = 3
//
// if it returns anything except e_ok, val will be set to zero
// commas are ignored (so you can put 100,000)
// this is _unsigned_ only, minus symbol causes e_bad_digit
// leading whitespace is skipped

int str_to_u(char const *src, uint32 &val)
{
    // skip leading whitespace
    while(isspace(*src)) {
        src += 1;
    }

    uint32 v = 0;      // result
    int rc = e_ok;     // return code
    int digits = 0;    // count the digits to make sure there are any
    while(true) {
        int c = *src++;
        if(c == 0) {
            break;
        }
        if(c == ',') {    // all commas are ignored
            continue;
        }
        c -= '0';
        if(c < 0 || c > 9) {
            rc = e_bad_digit;
            break;
        }
        digits += 1;
        uint32 old_v = v;
        v = v * 10 + c;
        if(v < old_v) {
            rc = e_overflow;
            break;
        }
    }
    if(rc == e_ok && digits == 0) {
        rc = e_empty_string;
    }
    if(rc != e_ok) {    // val is set to 0 on any error
        v = 0;
    }
    val = v;
    return rc;
}
