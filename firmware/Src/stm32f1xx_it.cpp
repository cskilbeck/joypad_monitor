#include "stm32f1xx_hal.h"
#include "stm32f1xx.h"
#include "stm32f1xx_it.h"
#include "util.h"

extern PCD_HandleTypeDef hpcd_USB_FS;

extern "C" {

void NMI_Handler(void)
{
}

void HardFault_Handler(void)
{
    while(1) {
        GPIOC->BSRR = 1 << 13;
        for(int i = 0; i < 1000000; ++i) {
        }
        GPIOC->BSRR = 1 << (13 + 16);
        for(int i = 0; i < 500000; ++i) {
        }
    }
}

void MemManage_Handler(void)
{
    while(1) {
    }
}

void BusFault_Handler(void)
{
    while(1) {
    }
}

void UsageFault_Handler(void)
{
    while(1) {
    }
}

void SVC_Handler(void)
{
}

void DebugMon_Handler(void)
{
}

void PendSV_Handler(void)
{
}

void SysTick_Handler(void)
{
    HAL_IncTick();
    HAL_SYSTICK_IRQHandler();
}

void USB_LP_CAN1_RX0_IRQHandler(void)
{
    HAL_PCD_IRQHandler(&hpcd_USB_FS);
}

void TIM1_UP_IRQHandler(void)
{
    TIM1->SR = ~TIM_IT_UPDATE;
    timer_ticks += 1;
    process_ticks += 1;

    // if user callback installed, call it
    if(timer_callback != null) {
        timer_callback();
    }
}

// this is called when TIM2 wraps over 0xffff
void TIM2_IRQHandler(void)
{
    TIM2->SR = 0;
    timer2 += 1;
}

}    // extern "C"
