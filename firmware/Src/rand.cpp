
#include <stdint.h>
#include "rand.h"
#include "util.h"

//////////////////////////////////////////////////////////////////////

static uint64_t state = 0x4d595df4d0f33173;    // Or something seed-dependent
static uint64_t const multiplier = 6364136223846793005u;
static uint64_t const increment = 1442695040888963407u;    // Or an arbitrary odd constant

//////////////////////////////////////////////////////////////////////

uint32_t random()
{
    uint64_t x = state;
    unsigned count = (unsigned)(x >> 59);    // 59 = 64 - 5
    state = x * multiplier + increment;
    x ^= x >> 18;                                 // 18 = (64 - 27)/2
    return rotr32((uint32_t)(x >> 27), count);    // 27 = 32 - 5
}

//////////////////////////////////////////////////////////////////////

void random_init(uint64_t seed)
{
    state = seed + increment;
    (void)random();
}

namespace rnd {

/*  Written in 2018 by David Blackman and Sebastiano Vigna (vigna@acm.org)

To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty.

See <http://creativecommons.org/publicdomain/zero/1.0/>. */

/* This is xoshiro128** 1.0, our 32-bit all-purpose, rock-solid generator. It
   has excellent (sub-ns) speed, a state size (128 bits) that is large
   enough for mild parallelism, and it passes all tests we are aware of.

   For generating just single-precision (i.e., 32-bit) floating-point
   numbers, xoshiro128+ is even faster.

   The state must be seeded so that it is not everywhere zero. */

static uint32_t s[4] = 
{
    0x1234567,
    0xabcdef0,
    0x9876543,
    0xfedcba5
};

uint32_t next()
{
    uint32_t const result_starstar = rotl32(s[0] * 5, 7) * 9;
    uint32_t const t = s[1] << 9;
    s[2] ^= s[0];
    s[3] ^= s[1];
    s[1] ^= s[2];
    s[0] ^= s[3];
    s[2] ^= t;
    s[3] = rotl32(s[3], 11);
    return result_starstar;
}

/* This is the jump function for the generator. It is equivalent
   to 2^64 calls to next(); it can be used to generate 2^64
   non-overlapping subsequences for parallel computations. */

static const uint32_t JUMP[] = { 0x8764000b, 0xf542d2d3, 0x6fa035c3, 0x77f2db5b };

void seed(uint64 seed)
{
    s[0] = uint32(seed);
    s[1] = uint32(seed >> 32);
}

void init()
{
    uint32_t s0 = 0;
    uint32_t s1 = 0;
    uint32_t s2 = 0;
    uint32_t s3 = 0;
    for(int i = 0; i < sizeof JUMP / sizeof *JUMP; i++)
        for(int b = 0; b < 32; b++) {
            if(JUMP[i] & (1U << b)) {
                s0 ^= s[0];
                s1 ^= s[1];
                s2 ^= s[2];
                s3 ^= s[3];
            }
            next();
        }

    s[0] = s0;
    s[1] = s1;
    s[2] = s2;
    s[3] = s3;
}

}    // namespace rand
