#include "usbd_cdc_if.h"

extern char printf_buffer[];

#define APP_RX_DATA_SIZE 1000
#define APP_TX_DATA_SIZE 1000

uint8_t UserRxBufferFS[APP_RX_DATA_SIZE];
uint8_t UserTxBufferFS[APP_TX_DATA_SIZE];

extern USBD_HandleTypeDef hUsbDeviceFS;

static int8_t CDC_Init_FS(void);
static int8_t CDC_DeInit_FS(void);
static int8_t CDC_Control_FS(uint8_t cmd, uint8_t *pbuf, uint16_t length);
static int8_t CDC_Receive_FS(uint8_t *pbuf, uint32_t *Len);

USBD_CDC_ItfTypeDef USBD_Interface_fops_FS = { CDC_Init_FS, CDC_DeInit_FS, CDC_Control_FS, CDC_Receive_FS };

static uint8_t input_buffer[1024];
static uint32_t input_len = 0;

/*********************************************************************************/
/* Line Coding Structure                                                         */
/*------------------------------------------------------------------------------ */
/* Offset | Field       | Size | Value  | Description                            */
/*------------------------------------------------------------------------------ */
/* 0      | dwDTERate   |  4   | Number | Data terminal rate, in bits per second */
/* 4      | bCharFormat |  1   | Number | Stop bits                              */
/*                                        0 - 1 Stop bit                         */
/*                                        1 - 1.5 Stop bits                      */
/*                                        2 - 2 Stop bits                        */
/* 5      | bParityType | 1    | Number | Parity                                 */
/*                                        0 - None                               */
/*                                        1 - Odd                                */
/*                                        2 - Even                               */
/*                                        3 - Mark                               */
/*                                        4 - Space                              */
/* 6     | bDataBits    |  1   | Number Data bits (5, 6, 7, 8 or 16).            */
/*********************************************************************************/

// static char const *parity_names[] = {
//    "N",
//    "O",
//    "E",
//    "M",
//    "S"
//};

// static char const *stop_bit_names[] = {
//    "1",
//    "1.5",
//    "2"
//};

#pragma pack(push, 1)
typedef struct line_coding
{
    uint32_t baud_rate;
    uint8_t stop_bits;
    uint8_t parity;
    uint8_t data_bits;
} line_coding;
#pragma pack(pop)

static line_coding current_line_coding = { 115200, 0, 0, 8 };    // 115.2K, 8N1 default line coding

extern void on_break();
extern void on_line_received(uint8_t *line, uint32_t len);
extern void on_connected();

static int8_t CDC_Init_FS(void)
{
    USBD_CDC_SetTxBuffer(&hUsbDeviceFS, UserTxBufferFS, 0);
    USBD_CDC_SetRxBuffer(&hUsbDeviceFS, UserRxBufferFS);
    return (USBD_OK);
}

static int8_t CDC_DeInit_FS(void)
{
    return (USBD_OK);
}

static int8_t CDC_Control_FS(uint8_t cmd, uint8_t *pbuf, uint16_t length)
{
    switch(cmd) {
    case CDC_SEND_ENCAPSULATED_COMMAND:
        break;
    case CDC_GET_ENCAPSULATED_RESPONSE:
        break;
    case CDC_SET_COMM_FEATURE:
        break;
    case CDC_GET_COMM_FEATURE:
        break;
    case CDC_CLEAR_COMM_FEATURE:
        break;
    case CDC_SET_LINE_CODING:
        memcpy(&current_line_coding, pbuf, 7);
        if(current_line_coding.data_bits > 0) {
            on_connected();
        }
        break;
    case CDC_GET_LINE_CODING:
        memcpy(pbuf, &current_line_coding, 7);
        break;
    case CDC_SET_CONTROL_LINE_STATE:
        break;
    case CDC_SEND_BREAK:
        break;
    default:
        break;
    }
    return (USBD_OK);
}

static int8_t CDC_Receive_FS(uint8_t *Buf, uint32_t *Len)
{
    uint8_t *p = Buf;
    uint32_t got = *Len;
    for(uint32_t i = 0; i < got; ++i) {
        uint8_t c = *p++;
        if(c == 3) {
            // they pressed ctrl-c
            on_break();
        } else if(input_len < 1023) {
            input_buffer[input_len++] = c;
        }
        if(c == '\r') {
            input_buffer[input_len] = 0;
            on_line_received(input_buffer, input_len);
            input_len = 0;
        }
    }
    USBD_CDC_SetRxBuffer(&hUsbDeviceFS, &Buf[0]);
    USBD_CDC_ReceivePacket(&hUsbDeviceFS);
    return (USBD_OK);
}

uint8_t CDC_Transmit_FS(uint8_t *Buf, uint16_t Len)
{
    uint8_t result = USBD_OK;
    USBD_CDC_HandleTypeDef *hcdc = (USBD_CDC_HandleTypeDef *)hUsbDeviceFS.pClassData;
    if(hcdc->TxState != 0) {
        return USBD_BUSY;
    }
    USBD_CDC_SetTxBuffer(&hUsbDeviceFS, Buf, Len);

    // don't lock up if putty is closed...
    if(hUsbDeviceFS.dev_state != USBD_STATE_CONFIGURED || hUsbDeviceFS.ep0_state != USBD_EP0_STATUS_OUT) {
        return USBD_FAIL;
    }
    return USBD_CDC_TransmitPacket(&hUsbDeviceFS);
}
