#pragma once

//////////////////////////////////////////////////////////////////////

struct config_var;

//////////////////////////////////////////////////////////////////////
// base for all activities

struct process_base
{
    process_base();
    virtual char const *name() = 0;
    virtual void update() = 0;
    virtual void on_command(int argc, char **argv) = 0;
    virtual void on_break() = 0;
    virtual void prompt();
    void end_process();
};

//////////////////////////////////////////////////////////////////////
// CLI process - accept commands and process them while reading buttons/sensor

struct cli_process : process_base
{
    cli_process();
    virtual char const *name();
    virtual void on_command(int argc, char **argv);
    virtual void on_break();
    virtual uint32 read_buttons();
    virtual void release_buttons();
    virtual void on_went_black();
    virtual void update();
    char const *separator;

    config_var *trigger_button_var;
    int records;
    int display_time;
    uint32 old_buttons;
    uint32 old_light;
    bool freeze;
};

//////////////////////////////////////////////////////////////////////
// test process - dumping ground for testing stuff

struct test_process : process_base
{
    test_process();
    virtual char const *name();
    virtual void on_break();
    virtual void on_command(int argc, char **argv);
    virtual void update();

    int shift;
    uint32 digits;
};

//////////////////////////////////////////////////////////////////////
// auto process - run an auto sequence

struct auto_process : cli_process
{
    auto_process();
    int get_delay();
    virtual char const *name();
    virtual void on_break();
    virtual uint32 read_buttons();
    virtual void on_went_black();
    virtual void on_command(int argc, char **argv);
    virtual void update();
    virtual void release_buttons();

    bool started;
    int delay;
    int num_samples;
    int max_samples;
    int min_delay;
    int max_delay;
    uint32 auto_buttons;
    uint32 start_buttons;
};

//////////////////////////////////////////////////////////////////////
// admin

namespace process {

extern int counter;
extern process_base *current_process;
extern byte process_object[64];    // must be at least as big as sizeof(largest process object)

//////////////////////////////////////////////////////////////////////

template <typename T> void start()
{
    counter += 1;
    current_process = new(process_object) T();
}

}    // namespace process
