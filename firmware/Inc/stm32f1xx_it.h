#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f1xx_hal.h"
#include "main.h"

void NMI_Handler(void);
void HardFault_Handler(void);
void MemManage_Handler(void);
void BusFault_Handler(void);
void UsageFault_Handler(void);
void SVC_Handler(void);
void DebugMon_Handler(void);
void PendSV_Handler(void);
void SysTick_Handler(void);
void USB_LP_CAN1_RX0_IRQHandler(void);

// user callback for 10 KHz timer - don't do anything that takes more than a few microseconds in here!
// set it to null to disable it
typedef void (*timer_callback_t)();
extern timer_callback_t timer_callback;

#ifdef __cplusplus
}
#endif
