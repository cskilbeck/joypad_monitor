#pragma once

//////////////////////////////////////////////////////////////////////
// a CLI command

struct command
{
    virtual char const *name() = 0;
    virtual char const *description() = 0;
    virtual char const *help() = 0;
    virtual void handle(int argc, char **argv) = 0;

    static command *find(char const *name);
};
