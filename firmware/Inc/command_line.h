#pragma once

//////////////////////////////////////////////////////////////////////
// for processing user inputs

struct command_line
{
    char buffer[1024];
    char *argv[16];
    int argc;

    int process_args(char *src, int len)
    {
        argc = 0;
        char *buffer_ptr = buffer;

        while(true) {
            while(*src != 0 && isspace(*src)) {
                src++;
            }
            if(argc == 16 || *src == 0) {
                break;
            }
            argv[argc++] = buffer_ptr;
            while(*src != 0 && !isspace(*src)) {
                *buffer_ptr++ = *src++;
            }
            *buffer_ptr++ = 0;
        }
        return argc;
    }
};
