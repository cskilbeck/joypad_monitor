#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "usbd_def.h"

extern USBD_DescriptorsTypeDef FS_Desc;

#ifdef __cplusplus
}
#endif
