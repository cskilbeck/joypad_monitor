#pragma once

//////////////////////////////////////////////////////////////////////

struct config_var
{
    config_var()
    {
    }

    uint32 value;

    virtual byte id() const = 0;
    virtual char const *name() = 0;
    virtual char const *type() = 0;
    virtual char const *description() = 0;
    virtual bool set(char const *text) = 0;
    virtual uint32 factory_default() const = 0;

    // format a value in the style of this type of config var
    virtual void print_val(char buffer[16], uint32 val) = 0;

    // all the config vars
    static config_var *config_vars[];
    static int const num_config_vars;

    // find a config var (case insensitive)
    static config_var *find(char const *name);

    static void save_all();
    static void load_all();
};
