#pragma once

//////////////////////////////////////////////////////////////////////
// timer.h

// timer_ticks and global_ticks are incremented 10 times per millisecond by the timer interrupt
extern volatile uint32 timer_ticks;
extern volatile uint32_t process_ticks;
