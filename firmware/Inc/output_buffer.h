#pragma once

//////////////////////////////////////////////////////////////////////
// printf to usb admin

template <int N> struct output_buffer_t
{
    uint32 len;
    uint8_t buffer[N];

    virtual void flush() = 0;

    void add_char(char c)
    {
        buffer[len++] = c;
        if(len == N) {
            flush();
            // assert(empty());    // flush() should call reset()
        }
    }

    bool empty() const
    {
        return len == 0;
    }

    void reset()
    {
        len = 0;
    }

    output_buffer_t()
    {
        reset();
    }
};
