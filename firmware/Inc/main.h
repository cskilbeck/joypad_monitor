#pragma once

#define PAD_A_GPIO_Port GPIOA
#define PAD_B_GPIO_Port GPIOA
#define PAD_X_GPIO_Port GPIOA
#define PAD_Y_GPIO_Port GPIOA

#define LED_A_GPIO_Port GPIOA
#define LED_B_GPIO_Port GPIOA
#define LED_X_GPIO_Port GPIOA
#define LED_Y_GPIO_Port GPIOA

#define LED_SNS_GPIO_Port GPIOA

#define D00_GPIO_Port GPIOB
#define D01_GPIO_Port GPIOB
#define D02_GPIO_Port GPIOB
#define D22_GPIO_Port GPIOB
#define D23_GPIO_Port GPIOB
#define D30_GPIO_Port GPIOB
#define D31_GPIO_Port GPIOB
#define D32_GPIO_Port GPIOB
#define D33_GPIO_Port GPIOB
#define D03_GPIO_Port GPIOB
#define D10_GPIO_Port GPIOB
#define D11_GPIO_Port GPIOB
#define D12_GPIO_Port GPIOB
#define D13_GPIO_Port GPIOB
#define D20_GPIO_Port GPIOB
#define D21_GPIO_Port GPIOB

#define LED_DBG1_GPIO_Port GPIOC
#define DP2_GPIO_Port GPIOC
#define IN_SNS_GPIO_Port GPIOC

#define PAD_A_Pin GPIO_PIN_0
#define PAD_B_Pin GPIO_PIN_1
#define PAD_X_Pin GPIO_PIN_2
#define PAD_Y_Pin GPIO_PIN_3

#define LED_A_Pin GPIO_PIN_4
#define LED_B_Pin GPIO_PIN_5
#define LED_X_Pin GPIO_PIN_6
#define LED_Y_Pin GPIO_PIN_7

#define LED_SNS_Pin GPIO_PIN_15

#define D00_Pin GPIO_PIN_0
#define D01_Pin GPIO_PIN_1
#define D02_Pin GPIO_PIN_2
#define D22_Pin GPIO_PIN_10
#define D23_Pin GPIO_PIN_11
#define D30_Pin GPIO_PIN_12
#define D31_Pin GPIO_PIN_13
#define D32_Pin GPIO_PIN_14
#define D33_Pin GPIO_PIN_15
#define D03_Pin GPIO_PIN_3
#define D10_Pin GPIO_PIN_4
#define D11_Pin GPIO_PIN_5
#define D12_Pin GPIO_PIN_6
#define D13_Pin GPIO_PIN_7
#define D20_Pin GPIO_PIN_8
#define D21_Pin GPIO_PIN_9

#define LED_DBG1_Pin GPIO_PIN_13
#define DP2_Pin GPIO_PIN_14
#define IN_SNS_Pin GPIO_PIN_15
