#pragma once

//////////////////////////////////////////////////////////////////////

#include <stdint.h>

//////////////////////////////////////////////////////////////////////

typedef unsigned char byte;
typedef unsigned char uint8;
typedef signed char int8;
typedef unsigned short uint16;
typedef signed short int16;
typedef unsigned int uint32;
typedef signed int int32;
typedef unsigned long long uint64;
typedef signed long long int64;

typedef unsigned int uint;

#define null 0

#define countof(x) (sizeof(x) / sizeof((x)[0]))

// low 16 bits in TIM2->CNT, when that wraps, this is incremented
extern volatile uint32 timer2;

// these are both incremented at 10KHz
extern volatile uint32 timer_ticks;
extern volatile uint32 process_ticks;

//////////////////////////////////////////////////////////////////////
// murmur hash from
// https://github.com/aappleby/smhasher/blob/master/src/MurmurHash3.h

uint32 MurmurHash3(const uint8_t *key, uint32 len, uint32 seed = 0);

//////////////////////////////////////////////////////////////////////
// placement new

inline void *operator new(uint32, void *p) throw()
{
    return p;
}

//////////////////////////////////////////////////////////////////////
// is it a whitespace char

inline int isspace(char c)
{
    return c == ' ' || c == '\r' || c == '\n' || c == '\t';
}

//////////////////////////////////////////////////////////////////////
// convert a char to uppercase

inline int toupper(int ch)
{
    return (ch >= 'a' && ch <= 'z') ? (ch - ('a' - 'A')) : ch;
}

//////////////////////////////////////////////////////////////////////

inline int isdigit(int ch)
{
    return ch >= '0' && ch <= '9';
}

//////////////////////////////////////////////////////////////////////
// case insensitive string compare

inline int stricmp(char const *s1, char const *s2)
{
    while(*s2 != 0 && toupper(*s1) == toupper(*s2)) {
        s1++;
        s2++;
    }
    return (int)(toupper(*s1) - toupper(*s2));
}

//////////////////////////////////////////////////////////////////////

static inline uint32 rotr32(uint32 x, unsigned r)
{
    uint32 result;
#if defined(__CC_ARM)
    __asm("ROR result, x, r");
#else
    asm("ror %0,%1,%2" : "=r"(result) : "r"(x), "r"(r));
#endif
    return result;
}

//////////////////////////////////////////////////////////////////////

inline void divmod(uint32 val, uint32 denom, uint32 &div, uint32 &mod)
{
#if defined(__CC_ARM)
    __asm("UDIV div,val,denom");       // div = val / denom
    __asm("MLS mod,denom,div,val");    // mod = val % denom (val - (div * denom))
#else
    asm("UDIV %0,%1,%2\n" : "=r"(div) : "r"(val), "r"(denom));
    asm("MLS %0,%1,%2,%3" : "=r"(mod) : "r"(denom), "r"(div), "r"(val));
#endif
}

//////////////////////////////////////////////////////////////////////

static inline uint32 rotl32(uint32 x, int8_t r)
{
    return rotr32(x, 32 - r);
}

//////////////////////////////////////////////////////////////////////
// convert string to uint32
// leading whitespace ignored
// comma separators allowed
// all other non-digit chars cause e_bad_digit
// if there are no digits, returns e_empty_string

enum { e_ok = 0, e_empty_string = 1, e_overflow = 2, e_bad_digit = 3 };

int str_to_u(char const *src, uint32 &val);

extern void button_mask_to_str(char *buffer, uint32 button_mask);
extern uint32 str_to_button_mask(char const *str);

//////////////////////////////////////////////////////////////////////

#define GPIO_LO(GPIO, mask) ((GPIO)->BSRR = ((mask) << 16))
#define GPIO_HI(GPIO, mask) ((GPIO)->BSRR = (mask))
#define GPIO_TOGGLE(GPIO, mask) ((GPIO)->ODR ^= (mask))

//////////////////////////////////////////////////////////////////////
// debug led admin

#define DEBUG_LED_OFF GPIO_HI(GPIOC, LED_DBG1_Pin)
#define DEBUG_LED_ON GPIO_LO(GPIOC, LED_DBG1_Pin)
#define DEBUG_LED_TOGGLE GPIO_TOGGLE(GPIOC, LED_DBG1_Pin)

//////////////////////////////////////////////////////////////////////
// console crap

#define CONSOLE_RESET "\033\143\033[3J\033\143"
#define BREAK ansi_red "\n** Break **\n" ansi_off
#define PROMPT ansi_green "%s" ansi_off " >"
