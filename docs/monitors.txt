Manufacturer    Model           Released    Display     Resolution  Rated       Actual      Refresh rate
                                            type                    response    response
                                                                    time        time

DELL            2001FP          2003        LCD         1600x1200   16ms        15ms        60Hz
LG              IPS224V         2013        IPS LCD     1920x1080   5ms         5ms         60Hz
Philips         223V7QHAB/00    2017        IPS LCD     1920x1080   5ms         5ms         60Hz
